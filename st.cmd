#!/usr/bin/env iocsh.bash
# Select the software release
require(mrfioc2)
require(cntpstats)
require(essioc)

epicsEnvSet("TOP", "$(E3_CMD_TOP)/..")
epicsEnvSet("LOCATION","B02")
epicsEnvSet("IOC", "ecdc-evg-01")
epicsEnvSet("SYS", "ECDC-B02:")
epicsEnvSet("DEV", "TS-EVG-01")
epicsEnvSet("SYSPV", "$(SYS)$(DEV)")
epicsEnvSet("ENGINEER","Nicklas Holmberg")
epicsEnvSet("MainEvt" "14")
epicsEnvSet("HeartBeatEvt"   "122")
epicsEnvSet("ESSEvtClockRate"  "88.0525")
epicsEnvSet("MTCA_3U_PCIID6",  "0b:00.0")

# -----------------------------------------------------------------------------
# loading databases
# -----------------------------------------------------------------------------
# E3 Common databases
#iocshLoad("$(essioc_DIR)/essioc.iocsh")
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# NTP monitor
iocshLoad("$(cntpstats_DIR)/cntpstats.iocsh","SYS=$(SYS), DEV=$(DEV):")

## 3U added epicsEnvSet("MTCA_3U_PCIID6",  "0b:00.0") to env-init.iocsh
iocshLoad("$(mrfioc2_DIR)/evm.iocsh", "P=$(SYSPV), PCIID=$(MTCA_3U_PCIID6)"


iocInit()


############### Configure RF input ##########
dbpf $(SYSPV):EvtClk-RFFreq-SP 88.0525
dbpf $(SYSPV):EvtClk-RFDiv-SP 1
dbpf $(SYSPV):EvtClk-Source-Sel "RF (Ext)"
# PLL needs time in order to synchronize
epicsThreadSleep 3
############### Configure RF input ##########

############## Configure front panel for evr 125 1 Hz ##############
# BUG: first set PPS input to NOT Front1,
dbpf $(SYSPV):1ppsInp-Sel "Sys Clk"
# Then set the PPS input for Front1
dbpf $(SYSPV):1ppsInp-Sel "Front1"
dbpf $(SYSPV):1ppsInp-MbbiDir_.TPRO 1
dbpf $(SYSPV):TrigEvt-4-TrigSrc-Sel "Front1"
dbpf $(SYSPV):TrigEvt-4-EvtCode-SP 125
dbpf $(SYSPV):SyncTimestamp-Cmd 1

############## Master Event Rate 14 Hz ##############
# # 14 Hz
dbpf $(SYSPV):Mxc-0-Prescaler-SP 6289464
dbpf $(SYSPV):TrigEvt-0-EvtCode-SP $(MainEvt)
dbpf $(SYSPV):TrigEvt-0-TrigSrc-Sel "Mxc0"

############## Sequencer Master Event Rate 14 Hz ##############
#iocshLoad "$(mrfioc2_DIR)/evg.r.iocsh", "P=$(SYSPV)"
# # Heart Beat 1 Hz
dbpf $(SYSPV):Mxc-7-Prescaler-SP 88052500
dbpf $(SYSPV):TrigEvt-7-EvtCode-SP $(HeartBeatEvt)
dbpf $(SYSPV):TrigEvt-7-TrigSrc-Sel "Mxc7"

# Set EVM as master (EVG) and run sequences
dbpf $(SYSPV):Enable-Sel "Ena Master"

#EOF
